/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.modelo.conexao.fabrica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Buger
 */
public class FabricadeConexao {

    public static Connection getConnection(String schemaMysql, String endereco, String user, String senha) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + endereco + ":3306/" + schemaMysql, user, senha);//Url de conexão

        } catch (SQLException ex) {
            System.out.println(ex);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        return null;
    }
}
