/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.modelo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Buger
 */
@Entity
@Table(name = "classificacao_aacc")
@NamedQueries({
    @NamedQuery(name = "ClassificacaoAacc.findAll", query = "SELECT c FROM ClassificacaoAacc c"),
    @NamedQuery(name = "ClassificacaoAacc.findByIdClassificacao", query = "SELECT c FROM ClassificacaoAacc c WHERE c.idClassificacao = :idClassificacao"),
    @NamedQuery(name = "ClassificacaoAacc.findByTitulo", query = "SELECT c FROM ClassificacaoAacc c WHERE c.titulo = :titulo")})
public class ClassificacaoAacc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idClassificacao")
    private Integer idClassificacao;
    @Basic(optional = false)
    @Column(name = "Titulo")
    private String titulo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idClassificacao")
    private List<Aacc> aaccList;

    public ClassificacaoAacc() {
    }

    public ClassificacaoAacc(Integer idClassificacao) {
        this.idClassificacao = idClassificacao;
    }

    public ClassificacaoAacc(Integer idClassificacao, String titulo) {
        this.idClassificacao = idClassificacao;
        this.titulo = titulo;
    }

    public Integer getIdClassificacao() {
        return idClassificacao;
    }

    public void setIdClassificacao(Integer idClassificacao) {
        this.idClassificacao = idClassificacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Aacc> getAaccList() {
        return aaccList;
    }

    public void setAaccList(List<Aacc> aaccList) {
        this.aaccList = aaccList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClassificacao != null ? idClassificacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassificacaoAacc)) {
            return false;
        }
        ClassificacaoAacc other = (ClassificacaoAacc) object;
        if ((this.idClassificacao == null && other.idClassificacao != null) || (this.idClassificacao != null && !this.idClassificacao.equals(other.idClassificacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thunderboard.modelo.modelos.ClassificacaoAacc[ idClassificacao=" + idClassificacao + " ]";
    }
    
}
