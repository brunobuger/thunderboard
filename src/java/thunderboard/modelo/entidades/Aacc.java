/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.modelo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Buger
 */
@Entity
@Table(name = "aacc")
@NamedQueries({
    @NamedQuery(name = "Aacc.findAll", query = "SELECT a FROM Aacc a"),
    @NamedQuery(name = "Aacc.findByIdAACC", query = "SELECT a FROM Aacc a WHERE a.idAACC = :idAACC"),
    @NamedQuery(name = "Aacc.findByNome", query = "SELECT a FROM Aacc a WHERE a.nome = :nome"),
    @NamedQuery(name = "Aacc.findByDataInicio", query = "SELECT a FROM Aacc a WHERE a.dataInicio = :dataInicio"),
    @NamedQuery(name = "Aacc.findByDataFim", query = "SELECT a FROM Aacc a WHERE a.dataFim = :dataFim"),
    @NamedQuery(name = "Aacc.findByDescricao", query = "SELECT a FROM Aacc a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "Aacc.findByLimiteHora", query = "SELECT a FROM Aacc a WHERE a.limiteHora = :limiteHora")})
public class Aacc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAACC")
    private Integer idAACC;
    @Basic(optional = false)
    @Column(name = "Nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "DataInicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;
    @Basic(optional = false)
    @Column(name = "DataFim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;
    @Basic(optional = false)
    @Column(name = "Descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "LimiteHora")
    private int limiteHora;
    @JoinColumn(name = "idPeriodo", referencedColumnName = "idPeriodo")
    @ManyToOne(optional = false)
    private Periodo idPeriodo;
    @JoinColumn(name = "idUsuario", referencedColumnName = "idUsuario")
    @ManyToOne(optional = false)
    private Usuario idUsuario;
    @JoinColumn(name = "idDocumento", referencedColumnName = "idDocumento")
    @ManyToOne(optional = false)
    private CategoriaDocumento idDocumento;
    @JoinColumn(name = "idClassificacao", referencedColumnName = "idClassificacao")
    @ManyToOne(optional = false)
    private ClassificacaoAacc idClassificacao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAACC")
    private List<Evento> eventoList;

    public Aacc() {
    }

    public Aacc(Integer idAACC) {
        this.idAACC = idAACC;
    }

    public Aacc(Integer idAACC, String nome, Date dataInicio, Date dataFim, String descricao, int limiteHora) {
        this.idAACC = idAACC;
        this.nome = nome;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.descricao = descricao;
        this.limiteHora = limiteHora;
    }

    public Integer getIdAACC() {
        return idAACC;
    }

    public void setIdAACC(Integer idAACC) {
        this.idAACC = idAACC;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getLimiteHora() {
        return limiteHora;
    }

    public void setLimiteHora(int limiteHora) {
        this.limiteHora = limiteHora;
    }

    public Periodo getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Periodo idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public CategoriaDocumento getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(CategoriaDocumento idDocumento) {
        this.idDocumento = idDocumento;
    }

    public ClassificacaoAacc getIdClassificacao() {
        return idClassificacao;
    }

    public void setIdClassificacao(ClassificacaoAacc idClassificacao) {
        this.idClassificacao = idClassificacao;
    }

    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAACC != null ? idAACC.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aacc)) {
            return false;
        }
        Aacc other = (Aacc) object;
        if ((this.idAACC == null && other.idAACC != null) || (this.idAACC != null && !this.idAACC.equals(other.idAACC))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thunderboard.modelo.modelos.Aacc[ idAACC=" + idAACC + " ]";
    }
    
}
