/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.modelo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Buger
 */
@Entity
@Table(name = "categoria_documento")
@NamedQueries({
    @NamedQuery(name = "CategoriaDocumento.findAll", query = "SELECT c FROM CategoriaDocumento c"),
    @NamedQuery(name = "CategoriaDocumento.findByIdDocumento", query = "SELECT c FROM CategoriaDocumento c WHERE c.idDocumento = :idDocumento"),
    @NamedQuery(name = "CategoriaDocumento.findByTitulo", query = "SELECT c FROM CategoriaDocumento c WHERE c.titulo = :titulo")})
public class CategoriaDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDocumento")
    private Integer idDocumento;
    @Basic(optional = false)
    @Column(name = "Titulo")
    private String titulo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDocumento")
    private List<Aacc> aaccList;

    public CategoriaDocumento() {
    }

    public CategoriaDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public CategoriaDocumento(Integer idDocumento, String titulo) {
        this.idDocumento = idDocumento;
        this.titulo = titulo;
    }

    public Integer getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Integer idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Aacc> getAaccList() {
        return aaccList;
    }

    public void setAaccList(List<Aacc> aaccList) {
        this.aaccList = aaccList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumento != null ? idDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaDocumento)) {
            return false;
        }
        CategoriaDocumento other = (CategoriaDocumento) object;
        if ((this.idDocumento == null && other.idDocumento != null) || (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "thunderboard.modelo.modelos.CategoriaDocumento[ idDocumento=" + idDocumento + " ]";
    }
    
}
