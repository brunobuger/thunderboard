/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.modelo.DAO;


import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import javax.persistence.*;
import javax.persistence.criteria.*;

/**
 *
 * @author Buger
 * BugerDAO 1.0
 */


public class DAOBuger implements Serializable{
    
    public static final int SELECT_TYPE_NORMAL = 0;
    public static final int SELECT_TYPE_DISTINCT = 1;
    
    public static final int WHERE_TYPE_NORMAL = 0;
    public static final int WHERE_TYPE_BETWEEN = 1;
    public static final int WHERE_TYPE_LIKE_TOTAL = 2;//'%VALOR%'
    public static final int WHERE_TYPE_LIKE_PREFIXO = 3;//'%VALOR'    
    public static final int WHERE_TYPE_LIKE_SUFIXO = 4;//'VALOR%'
    public static final int WHERE_TYPE_NOT_LIKE_TOTAL = 5;//'%VALOR%'
    public static final int WHERE_TYPE_NOT_LIKE_PREFIXO = 6;//'%VALOR'    
    public static final int WHERE_TYPE_NOT_LIKE_SUFIXO = 7;//'VALOR%'
    
    public static final int ORDER_TYPE_NORMAL = 0;
    public static final int ORDER_TYPE_ASC = 1;
    public static final int ORDER_TYPE_DESC = 2;
    
    public static final int OPERATOR_TYPE_NORMAL = 0;
    public static final int OPERATOR_TYPE_OR = 1;
    public static final int OPERATOR_TYPE_AND_OR = 2;
    public static final int OPERATOR_TYPE_OR_AND = 3;
    
    private int SELECT_TYPE = 0;
    private int WHERE_TYPE = 0;
    private int ORDER_TYPE = 0;
    private int OPERATOR_TYPE = 0;
    
    private String colunaOrder;
    private String colunaDistinct;
    private String colunaBetween;
    private String[] atributosString = new String[2];
    private Integer[] atributosInt = new Integer[2];
    
    
    private String emf;
    
    /**
     * @param emf Nome da unidade de persistencia
     */
    public DAOBuger()
    {
    }
    public DAOBuger(String emf)
    {
        this.emf = emf;
    }    
    
    public EntityManager getEntityManager() {
        EntityManagerFactory factory = null;
        EntityManager entityManager = null;
        try {
        //Obtém o factory a partir da unidade de persistência.
        factory = Persistence.createEntityManagerFactory(getEmf());
        //Cria um entity manager.
        entityManager = factory.createEntityManager();
        //Fecha o factory para liberar os recursos utilizado.
        } finally {
            //factory.close();
        }
        return entityManager;
    }
    
    public boolean salvar(Object objetoModel) {
        boolean sucesso = true;
        EntityManager entityManager = getEntityManager();
        
        try {
        // Inicia uma transação com o banco de dados.
        entityManager.getTransaction().begin();
        
        // Verifica se o objeto ainda não está salva no banco de dados.
        if(objetoModel == null) {
            //Salva os dados do objeto.
            entityManager.persist(objetoModel);
            
        } else {
            //Atualiza os dados do objeto.
            objetoModel = entityManager.merge(objetoModel);
        }
        // Finaliza a transação.
        entityManager.getTransaction().commit();
        } catch(Exception e) {
            //new MensagemModal("Erro ao salvar "+objetoModel.getClass().getSimpleName(), MensagemModal.MSG_ERRO);
            sucesso = false;
        } finally {
            entityManager.close();            
        }
        //if(sucesso)
          //  new MensagemModal("Salvo com sucesso.", MensagemModal.MSG_AVISO);
        
        return sucesso;
    }
    
    /**
   * Método que apaga a pessoa do banco de dados.
   * DELETE
   * @param id
   */
  public void excluir(Object objetoModel) {
        EntityManager entityManager = getEntityManager();
        try {
        // Inicia uma transação com o banco de dados.
        entityManager.getTransaction().begin();
        String[][] atributosSetados = getAtributosSetados(objetoModel);
        // Consulta o objeto na base de dados através do seu ID.
        
        //objetoModel = entityManager.find(objetoModel.getClass());
        //new MensagemModal("Excluindo os dados de: " + objetoModel.toString(), MensagemModal.MSG_AVISO);
        // Remove o objeto da base de dados.
        entityManager.remove(objetoModel);
        // Finaliza a transação.
        entityManager.getTransaction().commit();
        } finally {
        entityManager.close();
        }
    }

  /**
   * Consulta pelo ID.
   * @param id
   * @return
   */
  public Object consultarPorId(Object objetoModel, Long id) {
        EntityManager entityManager = getEntityManager();
        Object o = null;
        try {
        //Consulta uma pessoa pelo seu ID.
        objetoModel = entityManager.find(objetoModel.getClass(), id);
        } finally {
        entityManager.close();
        }
        return objetoModel;
    }
    
    public int getCount(Object objetoModel) {
            long tempoIni = System.currentTimeMillis();
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object> cq = cb.createQuery(Object.class);
            Root<Object> root = (Root<Object>) cq.from(objetoModel.getClass());
            String[][] atributosSetados = getAtributosSetados(objetoModel);
            Predicate predicate[] = new Predicate[atributosSetados.length];
            System.out.println("Reflection "+(System.currentTimeMillis()-tempoIni));
            //PARAMETROS CONSULTA
            for(int i = 0; i < atributosSetados.length; i++)
            {
                Class cls;
                try {
                    //System.out.println(tipoAtributo);
                    cls = Class.forName(atributosSetados[i][0]);//TIPO ATRIBUTO
                } catch (ClassNotFoundException ex) {
                    System.out.println("102 - "+ex);
                    cls = String.class;
                }
                
                predicate[i] = cb.equal(root.get(atributosSetados[i][1]), atributosSetados[i][2]);//NOME E VALOR ATRIBUTO
            }
            cq.select(root);
            
                if(WHERE_TYPE == WHERE_TYPE_NORMAL)
                    cq.where(cb.and(predicate));
                if(WHERE_TYPE == WHERE_TYPE_BETWEEN)
                {
                    if((getAtributosString()[0] != null && getAtributosString()[1] != null) || (getAtributosInt()[0] != null && getAtributosInt()[1] != null))
                    {
                        if(getAtributosInt()[0] != null)
                        {
                            Expression<Integer> expression = root.get(getColunaBetween());
                            cq.where(cb.and(predicate), cb.between(expression, atributosInt[0], atributosInt[1]));
                        }
                        else
                        {
                            Expression<String> expression = root.get(getColunaBetween());
                            cq.where(cb.and(predicate), cb.between(expression, atributosString[0], atributosString[1]));
                        }
                    }             
                }
                
                if(ORDER_TYPE > ORDER_TYPE_NORMAL)
                {
                    if(ORDER_TYPE == ORDER_TYPE_ASC)
                        cq.orderBy(cb.asc(root.get(getColunaOrder())));
                    if(ORDER_TYPE == ORDER_TYPE_DESC)
                        cq.orderBy(cb.desc(root.get(getColunaOrder())));
                }
            TypedQuery<Object> query;
            List<Object> results = null;
            if(SELECT_TYPE == SELECT_TYPE_NORMAL)
            {
               query = entityManager.createQuery(cq.select(cb.count(root)));
            }
            else
            {
               query = entityManager.createQuery(cq);
               results = query.getResultList();
            }
            entityManager.close();
            
            
            if(SELECT_TYPE == SELECT_TYPE_DISTINCT)
            {
                List<Object> newResults = new ArrayList();
                try {
                
                Set<String> DistinctDados = new HashSet<String>(); 
                Iterator<Object> iterator= results.iterator();
                boolean controle;
                Class cls = Class.forName(objetoModel.getClass().getName());
                while(iterator.hasNext())
                {
                    controle = false;
                    Object showObjects = (Object) iterator.next();
                    String valorAtributo = String.valueOf(cls.getMethod("get"+getColunaDistinct().substring(0, 1).toUpperCase()+getColunaDistinct().substring(1, getColunaDistinct().length())).invoke(showObjects));
                    
                    for(Object obj: DistinctDados)
                    {
                        if(obj.equals(valorAtributo))
                            controle = true;
                    }
                    DistinctDados.add(valorAtributo);
                    if(!controle)
                    {
                        newResults.add(showObjects);
                    }
                }
                } catch (Exception ex) {
                    //new MensagemModal("Erro "+objetoModel.getClass().getSimpleName(), MensagemModal.MSG_ERRO);
                }
                results = newResults;
            }
            System.out.println("Resultado Query "+(System.currentTimeMillis()-tempoIni));
            if(SELECT_TYPE == SELECT_TYPE_NORMAL)
                return (int) Integer.valueOf(query.getSingleResult().toString());
            else
                return Integer.valueOf(results.size());
    }
    
    public List<Object> consultar(Object objetoModel) {
            boolean geral = true;
            //System.out.println(getEmf());
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object> cq = cb.createQuery(Object.class);
            Root<Object> root = (Root<Object>) cq.from(objetoModel.getClass());
            String[][] atributosSetados = getAtributosSetados(objetoModel);
            Predicate predicate[] = new Predicate[atributosSetados.length];
                        
            //PARAMETROS CONSULTA
            for(int i = 0; i < atributosSetados.length; i++)
            {
                geral = false;
                String tipoAtributo = atributosSetados[i][0];
                String nomeAtributo = atributosSetados[i][1];
                String valorAtributo = atributosSetados[i][2];
                Class cls;
                try {
                    //System.out.println(tipoAtributo);
                    cls = Class.forName(tipoAtributo);
                } catch (ClassNotFoundException ex) {
                    System.out.println("102 - "+ex);
                    cls = String.class;
                }
                predicate[i] = cb.equal(root.get(nomeAtributo), valorAtributo);
            }
            
                cq.select(root);
            
                if(!geral)
                {
                    if(WHERE_TYPE == WHERE_TYPE_NORMAL)
                        cq.where(cb.and(predicate));
                    if(WHERE_TYPE == WHERE_TYPE_BETWEEN)
                    {
                        if((getAtributosString()[0] != null && getAtributosString()[1] != null) || (getAtributosInt()[0] != null && getAtributosInt()[1] != null))
                        {
                            if(getAtributosInt()[0] != null)
                            {
                                Expression<Integer> expression = root.get(getColunaBetween());
                                cq.where(cb.and(predicate), cb.between(expression, atributosInt[0], atributosInt[1]));
                            }
                            else
                            {
                                Expression<String> expression = root.get(getColunaBetween());
                                cq.where(cb.and(predicate), cb.between(expression, atributosString[0], atributosString[1]));
                            }
                        }
                        //else
                           // new MensagemModal("ERRO: Insira 2 atributos.", MensagemModal.MSG_ERRO);
                    }
                }
                
                if(ORDER_TYPE > ORDER_TYPE_NORMAL)
                {
                    if(ORDER_TYPE == ORDER_TYPE_ASC)
                        cq.orderBy(cb.asc(root.get(getColunaOrder())));
                    if(ORDER_TYPE == ORDER_TYPE_DESC)
                        cq.orderBy(cb.desc(root.get(getColunaOrder())));
                }
            
            TypedQuery<Object> query = entityManager.createQuery(cq);
            
            
            entityManager.close();
            
            List<Object> results = query.getResultList();
            
            if(SELECT_TYPE == SELECT_TYPE_DISTINCT)
            {
                List<Object> newResults = new ArrayList();
                try {
                
                Set<String> DistinctDados = new HashSet<String>(); 
                Iterator<Object> iterator= results.iterator();
                boolean controle;
                Class cls = Class.forName(objetoModel.getClass().getName());
                while(iterator.hasNext())
                {
                    controle = false;
                    Object showObjects = (Object) iterator.next();
                    String valorAtributo = String.valueOf(cls.getMethod("get"+getColunaDistinct().substring(0, 1).toUpperCase()+getColunaDistinct().substring(1, getColunaDistinct().length())).invoke(showObjects));
                    
                    for(Object obj: DistinctDados)
                    {
                        if(obj.equals(valorAtributo))
                            controle = true;
                    }
                    DistinctDados.add(valorAtributo);
                    if(!controle)
                    {
                        newResults.add(showObjects);
                    }
                }
                } catch (Exception ex) {
                    //new MensagemModal("Erro de consulta "+objetoModel.getClass().getSimpleName(), MensagemModal.MSG_ERRO);
                }
                results = newResults;
            }
            return results;
    }
        
    
    private static String[][] getAtributosSetados(Object objetoModel)//escaneia o objeto e retorna TIPO*ATRIBUTO*VALOR
    {
        String[][] atributosP = new String[40][3];//setado para limite de 40 atributos na classe
        String tipoAtributo = "";
        String nomeAtributo = "";
        String valorAtributo = "";
        int linhaAtributo = 0;
        try {
            //PEGA TIPO E ATRIBUTO
            Class cls = Class.forName(objetoModel.getClass().getName());
            Field fieldlist[] = cls.getDeclaredFields();  
            for (int i = 1; i < fieldlist.length; i++) {
                Field fld = fieldlist[i];
                //System.out.println("nome atributo = " + fld.getName());  
                nomeAtributo = fld.getName();
                
                tipoAtributo = fld.getType().getName();
                
                //PEGAR VALOR DO ATRIBUTO
                valorAtributo = String.valueOf(cls.getMethod("get"+nomeAtributo.substring(0, 1).toUpperCase()+nomeAtributo.substring(1, nomeAtributo.length())).invoke(objetoModel));                
                
                
                if((valorAtributo.compareTo("null") != 0) && ((tipoAtributo.contains("String") == true && valorAtributo.compareTo("0") == 0) || valorAtributo.compareTo("0") != 0) && !(tipoAtributo.compareTo("double") == 0 && valorAtributo.compareTo("0.0") == 0))
                {
                        atributosP[linhaAtributo][0] = tipoAtributo;//TIPO
                        atributosP[linhaAtributo][1] = nomeAtributo;//ATRIBUTO
                        atributosP[linhaAtributo][2] = valorAtributo;//VALOR
                        linhaAtributo++;
                    //System.out.println(tipoAtributo+" - "+ nomeAtributo+" - "+valorAtributo);
                }
                if(tipoAtributo.compareTo("boolean") == 0)
                {
                    linhaAtributo--;
                }
                
            }
            
        }
        catch (Throwable e) {  
            System.err.println(e);  
        }
        
        String[][] atributos = new String[linhaAtributo][3];
        for(int count = 0; count < linhaAtributo; count++)
        {
            
            atributos[count][0] = atributosP[count][0];//TIPO
            atributos[count][1] = atributosP[count][1];//ATRIBUTO
            atributos[count][2] = atributosP[count][2];//VALOR
            //System.out.println(atributos[count][1]+""+atributos[count][2]);
        }
        
        return atributos;
    }

    /**
     * @return the SELECT_TYPE
     */
    public int getSELECT_TYPE() {
        return SELECT_TYPE;
    }

    /**
     * @param SELECT_TYPE the SELECT_TYPE to set
     */
    public void setSELECT_TYPE(int SELECT_TYPE) {
        this.SELECT_TYPE = SELECT_TYPE;
    }

    /**
     * @return the WHERE_TYPE
     */
    public int getWHERE_TYPE() {
        return WHERE_TYPE;
    }

    /**
     * @param WHERE_TYPE the WHERE_TYPE to set
     */
    public void setWHERE_TYPE(int WHERE_TYPE) {
        this.WHERE_TYPE = WHERE_TYPE;
    }

    /**
     * @return the ORDER_BY_TYPE
     */
    public int getORDER_TYPE() {
        return ORDER_TYPE;
    }

    /**
     * @param ORDER_BY_TYPE the ORDER_BY_TYPE to set
     */
    public void setORDER_TYPE(int ORDER_TYPE) {
        this.ORDER_TYPE = ORDER_TYPE;
    }

    /**
     * @return the OPERATOR_BY_TYPE
     */
    public int getOPERATOR_TYPE() {
        return OPERATOR_TYPE;
    }

    /**
     * @param OPERATOR_BY_TYPE the OPERATOR_BY_TYPE to set
     */
    public void setOPERATOR_TYPE(int OPERATOR_TYPE) {
        this.OPERATOR_TYPE = OPERATOR_TYPE;
    }

    /**
     * @return the atributoString
     */
    private String[] getAtributosString() {
        return atributosString;
    }

    /**
     * @param atributoString the atributoString to set
     */
    private void setAtributosString(String atributoString1, String atributoString2) {
        this.atributosString[0] = atributoString1;
        this.atributosString[1] = atributoString2;
    }

    /**
     * @return the atributoInt
     */
    private Integer[] getAtributosInt() {
        return atributosInt;
    }

    /**
     * @param atributoInt the atributoInt to set
     */
    private void setAtributosInt(int atributoInt1, int atributoInt2) {
        this.atributosInt[0] = atributoInt1;
        this.atributosInt[1] = atributoInt2;
    }

    
    public void setWHERE_BETWEEN(String coluna, int atr1, int atr2) {
        setAtributosInt(atr1, atr2);
        setColunaBetween(coluna);
        setWHERE_TYPE(DAOBuger.WHERE_TYPE_BETWEEN);
    }
    
    public void setWHERE_BETWEEN(String coluna, String atr1, String atr2) {
        setAtributosString(atr1, atr2);
        setColunaBetween(coluna);
        setWHERE_TYPE(DAOBuger.WHERE_TYPE_BETWEEN);
    }

    public void setSELECT_DISTINCT(String coluna) {
        setColunaDistinct(coluna);
        setSELECT_TYPE(DAOBuger.SELECT_TYPE_DISTINCT);
    }
    
    public void setORDER_ASC(String coluna) {
        setColunaOrder(coluna);
        setORDER_TYPE(DAOBuger.ORDER_TYPE_ASC);
    }
    public void setORDER_DESC(String coluna) {
        setColunaOrder(coluna);
        setORDER_TYPE(DAOBuger.ORDER_TYPE_DESC);
    }
    
    /**
     * @return the colunaBetween
     */
    private String getColunaBetween() {
        return colunaBetween;
    }

    /**
     * @param colunaBetween the colunaBetween to set
     */
    private void setColunaBetween(String colunaBetween) {
        this.colunaBetween = colunaBetween;
    }

    /**
     * @return the colunaDistinct
     */
    private String getColunaDistinct() {
        return colunaDistinct;
    }

    /**
     * @param colunaDistinct the colunaDistinct to set
     */
    private void setColunaDistinct(String colunaDistinct) {
        this.colunaDistinct = colunaDistinct;
    }

    /**
     * @return the colunaOrder
     */
    private String getColunaOrder() {
        return colunaOrder;
    }

    /**
     * @param colunaOrder the colunaOrder to set
     */
    private void setColunaOrder(String colunaOrder) {
        this.colunaOrder = colunaOrder;
    }

    /*public List<Object> consultarAntigo(Object objetoModel) {
            EntityManager em = getEntityManager();
            List result = new ArrayList();
            em.getTransaction().begin();
            String[][] atributosSetados = getAtributosSetados(objetoModel);
            String jpql;//o.age > :age"
            
            
            //SELECT_TYPES
            String distinct = "";
            
            jpql = "SELECT o FROM "+objetoModel.getClass().getName()+" o";
            
            System.out.println(jpql);
            
            for(int i = 0; i < atributosSetados.length; i++)
            {
                String tipoAtributo = atributosSetados[i][0];
                String nomeAtributo = atributosSetados[i][1];
                String valorAtributo = atributosSetados[i][2];
                
                //WHERE_TYPES
                if (i == 0) {
                    jpql += " WHERE o." + nomeAtributo + "=:" + nomeAtributo;
                } else {
                    jpql += " AND o." + nomeAtributo + "=:" + nomeAtributo;
                }
                
            }
                if(WHERE_TYPE == WHERE_TYPE_BETWEEN)
                {
                    if((getAtributosString()[0] != null && getAtributosString()[1] != null) || (getAtributosInt()[0] != null && getAtributosInt()[1] != null))
                    {
                        if(getAtributosInt()[0] != null)
                        {
                            if(atributosSetados.length == 0)
                            {
                                jpql += " WHERE o."+getColunaBetween()+" BETWEEN :valor1 AND :valor2";
                            }
                            else
                            {
                                jpql += " AND o."+getColunaBetween()+" BETWEEN :valor1 AND :valor2";
                            }
                        }
                        else
                        {
                            //System.out.println(getAtributosString()[0]+getAtributosString()[1]);
                            if(atributosSetados.length == 0)
                            {
                                jpql += " WHERE o."+getColunaBetween()+" BETWEEN :valor1 AND :valor2";
                            }
                            else
                            {
                                jpql += " AND o."+getColunaBetween()+" BETWEEN :valor1 AND :valor2";
                            }
                        }
                    }
                    else
                        System.out.println("ERRO: Insira 2 atributos.");
                }
            
                if(ORDER_TYPE > ORDER_TYPE_NORMAL)
                {
                    if(ORDER_TYPE == ORDER_TYPE_ASC)
                        jpql += " ORDER BY "+getColunaOrder()+" ASC";
                    if(ORDER_TYPE == ORDER_TYPE_DESC)
                        jpql += " ORDER BY "+getColunaOrder()+" DESC";
                }
                
            try {
                TypedQuery<Object> tquery = (TypedQuery<Object>) em.createQuery(jpql);
                for(int i = 0; i < atributosSetados.length; i++)
                {
                    String tipoAtributo = atributosSetados[i][0];
                    String nomeAtributo = atributosSetados[i][1];
                    String valorAtributo = atributosSetados[i][2];


                    tquery.setParameter(nomeAtributo, valorAtributo);

                    if(WHERE_TYPE == WHERE_TYPE_BETWEEN)
                    {
                        if((getAtributosString()[0] != null && getAtributosString()[1] != null) || (getAtributosInt()[0] != null && getAtributosInt()[1] != null))
                        {
                            if(getAtributosInt()[0] != null)
                            {
                                tquery.setParameter("valor1", getAtributosInt()[0]);
                                tquery.setParameter("valor2", getAtributosInt()[1]);
                            }
                            else
                            {
                                tquery.setParameter("valor1", getAtributosString()[0]);
                                tquery.setParameter("valor2", getAtributosString()[1]);
                            }
                        }
                        else
                            System.out.println("Erro: Insira 2 atributos.");
                    }
                    //tquery.setParameter(nomeAtributo, valorAtributo);
                }
                Query query = tquery;
                result = query.getResultList();
            } catch (Exception qe)
            {
                System.out.println("ERRO: O nome do atributo está errado.");
            }
            
            return result;
    }*/
    
    public static String getNomedaColunaId(Class classe) {
	String fieldName = null;
        Field[] fileds = classe.getDeclaredFields();  
        for (Field f : fileds) {  
            for(Annotation annotation : f.getAnnotations())
                if(annotation.annotationType().equals(Id.class)) {
                	fieldName = f.getName();
                	break;
                }
        }  
        return fieldName;  
    }

    /**
     * @return Retorna o nome da unidade de persistencia
     */
    public String getEmf() {
        return emf;
    }

    /**
     * @param emf Nome da unidade de persistencia
     */
    public void setEmf(String emf) {
        this.emf = emf;
    }
    
}
