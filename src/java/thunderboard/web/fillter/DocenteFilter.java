/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.fillter;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import thunderboard.web.controle.LoginManager;

/**
 *
 * @author Buger
 */
@WebFilter("/docente/*")
public class DocenteFilter implements Serializable, Filter{
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
        FilterChain chain) throws IOException, ServletException {
        // TODO Auto-generated method stub  
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        LoginManager userManager = (LoginManager) req.getSession().getAttribute("acesso");
        if (userManager == null || userManager.getUsuario().getNivel() != 2) {
            // Aqui retorno se não existir...  
            res.sendRedirect(req.getContextPath() + "/index.xhtml");
        } else {
            // Aqui continua se existir
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        
    }

    @Override
    public void destroy() {
        
    }
}
