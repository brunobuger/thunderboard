/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import thunderboard.modelo.entidades.Usuario;

/**
 *
 * @author Buger
 */
@ManagedBean(name = "Esqueceu")
@ViewScoped
public class LoginManagerForgot implements Serializable {

    private Usuario usuario = new Usuario();
    private UsuarioManager um = new UsuarioManager();
    private EmailManager em = new EmailManager();
    private String email;
    

    public void Envia() {
        try {
            usuario = um.verificaEmail(getEmail());
            if (usuario.getEmail().equals(getEmail())) {
                getEm().enviandoEmails(getUsuario());
                displaySucesso();
            } else {
                displayErro();
            }
        } catch (Exception ex) {
            displayErro();
            Logger.getLogger(LoginManagerForgot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void displayErro() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "E-mail nao encontrado!");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void displaySucesso() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "E-mail enviado com Sucesso!");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public boolean EnviarEmail() {
        //Editar o metodo de enviar email;
        return true;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the um
     */
    public UsuarioManager getUm() {
        return um;
    }

    /**
     * @param um the um to set
     */
    public void setUm(UsuarioManager um) {
        this.um = um;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the em
     */
    public EmailManager getEm() {
        return em;
    }

    /**
     * @param em the em to set
     */
    public void setEm(EmailManager em) {
        this.em = em;
    }
}