/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.ClassificacaoAacc;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "ClassificacaoAacc")
@SessionScoped
public class ClassificacaoAaccManager implements Serializable{
    private ClassificacaoAacc classificacaoAacc = new ClassificacaoAacc();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private List<ClassificacaoAacc> resultSetClassificacao;
    private List<String> listClassificacao;
    
    public ClassificacaoAaccManager(){
        selectAllClassificacoes();
    }
    
    public void selectAllClassificacoes(){
        resultSetClassificacao =  new ArrayList<ClassificacaoAacc>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<ClassificacaoAacc> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, ClassificacaoAacc.class);
        setResultSetClassificacao(easyCriteria.getResultList());
    }
    
    public List<String> autoCompletarClassificacao(String query){
        listClassificacao = new ArrayList<String>();
        for (int i = 0; i < resultSetClassificacao.size(); i++) {
            getListClassificacao().add("" + resultSetClassificacao.get(i).getTitulo());
        }
        return listClassificacao;
    }
    
    public ClassificacaoAacc selecionaClassificacaoList(int idClassificacao){
        ClassificacaoAacc c = new ClassificacaoAacc();
        for (int i = 0; i < getResultSetClassificacao().size(); i++) {
            if (getResultSetClassificacao().get(i).getIdClassificacao().equals(idClassificacao)) {
                c =  getResultSetClassificacao().get(i);
            }
        }
        return c;
    }
    
    public void excluirClassificacaoAacc(String nome){
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<ClassificacaoAacc> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, ClassificacaoAacc.class);
        try {
            easyCriteria.andEquals("nome", getClassificacaoAacc().getTitulo());
            setClassificacaoAacc(easyCriteria.getResultList().get(0));
            em.remove(getClassificacaoAacc());
        } catch (Exception e) {
            setClassificacaoAacc(new ClassificacaoAacc());
            System.out.println("Atividade nao encontrada.");
        }
    }

    /**
     * @return the classificacaoAacc
     */
    public ClassificacaoAacc getClassificacaoAacc() {
        return classificacaoAacc;
    }

    /**
     * @param classificacaoAacc the classificacaoAacc to set
     */
    public void setClassificacaoAacc(ClassificacaoAacc classificacaoAacc) {
        this.classificacaoAacc = classificacaoAacc;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the resultClassificacao
     */
    public List<ClassificacaoAacc> getResultSetClassificacao() {
        return resultSetClassificacao;
    }

    /**
     * @param resultClassificacao the resultClassificacao to set
     */
    public void setResultSetClassificacao(List<ClassificacaoAacc> resultSetClassificacao) {
        this.resultSetClassificacao = resultSetClassificacao;
    }

    /**
     * @return the listPeriodos
     */
    public List<String> getListClassificacao() {
        return listClassificacao;
    }

    /**
     * @param listPeriodos the listPeriodos to set
     */
    public void setListClassificacao(List<String> listClassificacao) {
        this.listClassificacao = listClassificacao;
    }
}
