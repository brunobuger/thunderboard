/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Evento;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "Evento")
@SessionScoped
public class EventoManager implements Serializable {

    private Evento evento = new Evento();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private boolean aprovado;
    private int pontos;
    private String feedBack;
    private int idAtividade;
    private Evento selectedEvento = new Evento();
    private int idEvento;
    private Evento eventoSelecionado;
    private List<Evento> resultSetEvento;
    private List<Evento> filtroEvento;
    private List<Integer> limiteHora;
    private List<Evento> AaccUsuario;
    

    public EventoManager() {
        selectAllEventos();
        aaccEnviadas();
    }

    public void selectAllEventos() {
        resultSetEvento = new ArrayList<Evento>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Evento> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Evento.class);
        setResultSetEvento(easyCriteria.getResultList());
    }

    public void cadastrarEvento() {
        try {
            if (getEvento() != null) {
                getEvento().setIdUsuario(LoginManager.logado);
                atribuiAtividade();
                getEvento().setFeedBack(" ");
                System.out.println(getEvento().getIdAACC().getIdAACC());

                EntityManager em = getDao().getEntityManager();
                em.getTransaction().begin();
                em.persist(getEvento());
                em.getTransaction().commit();
                System.out.println("cadastrou evento!!");

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade enviada!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Atividade não enviada!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void validarAacc() {
        System.out.println("FEEDBACK: " + getFeedBack());
        getSelectedEvento().setFeedBack(getFeedBack());
        System.out.println("FEEDBACK: " + getSelectedEvento().getFeedBack());
        try {
            EntityManager em = getDao().getEntityManager();
            Evento evt = em.find(Evento.class, getSelectedEvento().getIdEvento());
            em.getTransaction().begin();
            evt.setAceito(getSelectedEvento().getAceito());
            evt.setPontuacao(getSelectedEvento().getPontuacao());
            evt.setFeedBack(getSelectedEvento().getFeedBack());
            em.getTransaction().commit();
            System.out.println("validou atividade!");

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade avaliada!");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (Exception e) {
            System.out.println("ERRO: " + e);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Atividade não avaliada!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void atribuiAtividade() {
        AaccManager am = new AaccManager();
        getEvento().setIdAACC(am.retornaAaccSelecionada(getIdAtividade()));
        System.out.println("NOME ACC: " + getEvento().getIdAACC().getNome() + " - ID ACC: " + getEvento().getIdAACC().getIdAACC());
    }

    public void filtraEvento() {
        filtroEvento = new ArrayList<Evento>();
        atribuiAtividade();
        for (int i = 0; i < getResultSetEvento().size(); i++) {
            if (getResultSetEvento().get(i).getIdAACC().getIdAACC().equals(getEvento().getIdAACC().getIdAACC())) {
                getFiltroEvento().add(getResultSetEvento().get(i));
            }
        }
        System.out.println("SIZE: " + getFiltroEvento().size());
    }

    public void selecionaEvento() {

        for (int i = 0; i < getResultSetEvento().size(); i++) {
            if (getResultSetEvento().get(i).getIdEvento().equals(getIdEvento())) {
                setSelectedEvento(getResultSetEvento().get(i));
            }
        }
    }

    public void calcularPontos() {
        limiteHora = new ArrayList<Integer>();
        if (getSelectedEvento().getAceito()) {
            for (int i = 0; i < getSelectedEvento().getIdAACC().getLimiteHora(); i++) {
                getLimiteHora().add(i + 1);
            }
            System.out.println("EXECUTOU: " + getSelectedEvento().getIdAACC().getLimiteHora());
        } else {
            getLimiteHora().add(0);
        }
    }

    public List<Evento> aaccEnviadas() {
        AaccUsuario = new ArrayList<Evento>();
        for (int i = 0; i < getResultSetEvento().size(); i++) {
            if (getResultSetEvento().get(i).getIdUsuario().getIdUsuario().equals(LoginManager.logado.getIdUsuario())) {
                getAaccUsuario().add(getResultSetEvento().get(i));
            }
        }
        System.out.println("SIZE: " + getAaccUsuario().size());
        return getAaccUsuario();
    }

    public void teste() {
        System.out.println("EVENTO ID: " + getEventoSelecionado().getIdEvento());
    }

    /**
     * @return the evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the aprovado
     */
    public boolean isAprovado() {
        return aprovado;
    }

    /**
     * @param aprovado the aprovado to set
     */
    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    /**
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    /**
     * @return the feedBack
     */
    public String getFeedBack() {
        return feedBack;
    }

    /**
     * @param feedBack the feedBack to set
     */
    public void setFeedBack(String feedBack) {
        this.feedBack = feedBack;
    }

    /**
     * @return the atividade
     */
    public int getIdAtividade() {
        return idAtividade;
    }

    /**
     * @param atividade the atividade to set
     */
    public void setIdAtividade(int idAtividade) {
        this.idAtividade = idAtividade;
    }

    /**
     * @return the resultEvento
     */
    public List<Evento> getResultSetEvento() {
        return resultSetEvento;
    }

    /**
     * @param resultEvento the resultEvento to set
     */
    public void setResultSetEvento(List<Evento> resultSetEvento) {
        this.resultSetEvento = resultSetEvento;
    }

    /**
     * @return the filtroEvento
     */
    public List<Evento> getFiltroEvento() {
        return filtroEvento;
    }

    /**
     * @param filtroEvento the filtroEvento to set
     */
    public void setFiltroEvento(List<Evento> filtroEvento) {
        this.filtroEvento = filtroEvento;
    }

    /**
     * @return the selectedEvento
     */
    public Evento getSelectedEvento() {
        return selectedEvento;
    }

    /**
     * @param selectedEvento the selectedEvento to set
     */
    public void setSelectedEvento(Evento selectedEvento) {
        this.selectedEvento = selectedEvento;
    }

    /**
     * @return the eventoSelecionado
     */
    public Evento getEventoSelecionado() {
        return eventoSelecionado;
    }

    /**
     * @param eventoSelecionado the eventoSelecionado to set
     */
    public void setEventoSelecionado(Evento eventoSelecionado) {
        this.eventoSelecionado = eventoSelecionado;
    }

    /**
     * @return the idEvento
     */
    public int getIdEvento() {
        return idEvento;
    }

    /**
     * @param idEvento the idEvento to set
     */
    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    /**
     * @return the limiteHora
     */
    public List<Integer> getLimiteHora() {
        return limiteHora;
    }

    /**
     * @param limiteHora the limiteHora to set
     */
    public void setLimiteHora(List<Integer> limiteHora) {
        this.limiteHora = limiteHora;
    }

    /**
     * @return the AaccUsuario
     */
    public List<Evento> getAaccUsuario() {
        return AaccUsuario;
    }

    /**
     * @param AaccUsuario the AaccUsuario to set
     */
    public void setAaccUsuario(List<Evento> AaccUsuario) {
        this.AaccUsuario = AaccUsuario;
    }
}
