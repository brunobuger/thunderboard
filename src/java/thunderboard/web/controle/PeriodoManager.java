/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.sun.org.apache.xpath.internal.operations.Gte;
import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Periodo;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "Turma")
@SessionScoped
public class PeriodoManager implements Serializable {

    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private List<Periodo> resultSetPeriodo;
    private List<String> listPeriodos;
    private Periodo excluirSelectedPeriodo;
    private int idPeriodoExc;
    private Periodo cadastrarSelectedPeriodo;
    private String cadastrarPeriodoStringSemestre;
    private String cadastrarPeriodoStringAno;

    public PeriodoManager() {
        selectAllPeriodos();
    }

    public void selectAllPeriodos() {
        resultSetPeriodo = new ArrayList<Periodo>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Periodo> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Periodo.class);
        resultSetPeriodo = easyCriteria.getResultList();
    }

    public List<String> autoCompletarPeriodo(String query) {
        listPeriodos = new ArrayList<String>();
        for (int i = 0; i < resultSetPeriodo.size(); i++) {
            getListPeriodos().add("" + resultSetPeriodo.get(i).getSemestre() + "º semestre " + resultSetPeriodo.get(i).getAno());
        }
        return listPeriodos;
    }

    public Periodo selecionarPeriodo(int sem, short ano) {
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Periodo> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Periodo.class);
        Periodo p = new Periodo();
        try {
            easyCriteria.andEquals("semestre", sem);
            easyCriteria.andEquals("ano", ano);
            p = easyCriteria.getResultList().get(0);
        } catch (Exception e) {
        }
        return p;
    }

    public void excluir() {
        setExcluirSelectedPeriodo(selecionaPeriodoList(getIdPeriodoExc()));

        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Periodo> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Periodo.class);
        try {
            easyCriteria.andEquals("ano", getExcluirSelectedPeriodo().getAno());
            easyCriteria.andEquals("semestre", getExcluirSelectedPeriodo().getSemestre());
            setExcluirSelectedPeriodo(easyCriteria.getResultList().get(0));
            em.getTransaction().begin();
            em.remove(getExcluirSelectedPeriodo());
            em.getTransaction().commit();
            
            selectAllPeriodos();

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Período excluído!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Período não excluído!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void cadastrarAtribuirPeriodo() {
        String s1 = getCadastrarPeriodoStringSemestre();
        String s2 = getCadastrarPeriodoStringAno();
        setCadastrarSelectedPeriodo(new Periodo());
        getCadastrarSelectedPeriodo().setSemestre(Short.parseShort(s1));
        getCadastrarSelectedPeriodo().setAno(Integer.parseInt(s2));
    }

    public void cadastrarPeriodo() {
        cadastrarAtribuirPeriodo();
        try {
            if (getCadastrarSelectedPeriodo() != null) {
                System.out.println("ano: " + getCadastrarSelectedPeriodo().getAno() + " semestre: " + getCadastrarSelectedPeriodo().getSemestre());
                EntityManager em = getDao().getEntityManager();
                em.getTransaction().begin();
                em.persist(getCadastrarSelectedPeriodo());
                em.getTransaction().commit();
                
                selectAllPeriodos();

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Período cadastrado!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            System.out.println("crio essa merda.");
        } catch (Exception e) {
            System.out.println("ERRO: " + e);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Período não cadastrado!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }

    public Periodo selecionaPeriodoList(int idPeriodo) {
        Periodo c = new Periodo();
        for (int i = 0; i < getResultSetPeriodo().size(); i++) {
            if (getResultSetPeriodo().get(i).getIdPeriodo().equals(idPeriodo)) {
                c = getResultSetPeriodo().get(i);
            }
        }
        return c;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the resultPerido
     */
    public List<Periodo> getResultSetPeriodo() {
        return resultSetPeriodo;
    }

    /**
     * @param resultPerido the resultPerido to set
     */
    public void setResultSetPeriodo(List<Periodo> resultSetPeriodo) {
        this.resultSetPeriodo = resultSetPeriodo;
    }

    /**
     * @return the listPeriodos
     */
    public List<String> getListPeriodos() {
        return listPeriodos;
    }

    /**
     * @param listPeriodos the listPeriodos to set
     */
    public void setListPeriodos(List<String> listPeriodos) {
        this.listPeriodos = listPeriodos;
    }

    /**
     * @return the selectedPeriodo
     */
    public Periodo getSelectedPeriodo() {
        return getExcluirSelectedPeriodo();
    }

    /**
     * @param selectedPeriodo the selectedPeriodo to set
     */
    public void setSelectedPeriodo(Periodo selectedPeriodo) {
        this.setExcluirSelectedPeriodo(selectedPeriodo);
    }

    /**
     * @return the periodoString
     */
    public int getExluirPeriodoString() {
        return getIdPeriodoExc();
    }

    /**
     * @param periodoString the periodoString to set
     */
    public void setExluirPeriodoString(int periodoString) {
        this.setIdPeriodoExc(periodoString);
    }

    /**
     * @return the excluirSelectedPeriodo
     */
    public Periodo getExcluirSelectedPeriodo() {
        return excluirSelectedPeriodo;
    }

    /**
     * @param excluirSelectedPeriodo the excluirSelectedPeriodo to set
     */
    public void setExcluirSelectedPeriodo(Periodo excluirSelectedPeriodo) {
        this.excluirSelectedPeriodo = excluirSelectedPeriodo;
    }

    /**
     * @return the exlucirPeriodoString
     */
    public int getIdPeriodoExc() {
        return idPeriodoExc;
    }

    /**
     * @param exlucirPeriodoString the exlucirPeriodoString to set
     */
    public void setIdPeriodoExc(int idPeriodoExc) {
        this.idPeriodoExc = idPeriodoExc;
    }

    /**
     * @return the cadastrarSelectedPeriodo
     */
    public Periodo getCadastrarSelectedPeriodo() {
        return cadastrarSelectedPeriodo;
    }

    /**
     * @param cadastrarSelectedPeriodo the cadastrarSelectedPeriodo to set
     */
    public void setCadastrarSelectedPeriodo(Periodo cadastrarSelectedPeriodo) {
        this.cadastrarSelectedPeriodo = cadastrarSelectedPeriodo;
    }

    /**
     * @return the cadastrarPeriodoStringSemestre
     */
    public String getCadastrarPeriodoStringSemestre() {
        return cadastrarPeriodoStringSemestre;
    }

    /**
     * @param cadastrarPeriodoStringSemestre the cadastrarPeriodoStringSemestre
     * to set
     */
    public void setCadastrarPeriodoStringSemestre(String cadastrarPeriodoStringSemestre) {
        this.cadastrarPeriodoStringSemestre = cadastrarPeriodoStringSemestre;
    }

    /**
     * @return the cadastrarPeriodoStringAno
     */
    public String getCadastrarPeriodoStringAno() {
        return cadastrarPeriodoStringAno;
    }

    /**
     * @param cadastrarPeriodoStringAno the cadastrarPeriodoStringAno to set
     */
    public void setCadastrarPeriodoStringAno(String cadastrarPeriodoStringAno) {
        this.cadastrarPeriodoStringAno = cadastrarPeriodoStringAno;
    }
}
