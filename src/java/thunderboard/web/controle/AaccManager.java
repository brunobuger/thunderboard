/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Aacc;
import thunderboard.modelo.entidades.Periodo;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "Atividade")
@ViewScoped
public class AaccManager implements Serializable {

    private Aacc aacc = new Aacc();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private int idPeriodo;
    private int idClassificacao;
    private int idDocumento;
    private List<Aacc> resultSetAacc;
    private List<Aacc> filtroAacc;
    private List<Aacc> listPorUsuario;
    private List<Aacc> listPorPeriodo;
    private String text;
    private Aacc selectedAacc;
    private Periodo p;
    private String anexoImg = "";

    
    public AaccManager() {
        resultAacc();
        atividadPorUsuario();
    }

    public void cadastrarAacc() {
        atribuiPeriodo();
        dataHoraAtual();
        atribuiDocumento();
        atribuiClassificacao();
        getAacc().setIdUsuario(LoginManager.logado);
        try {
            if (getAacc() != null) {
                EntityManager em = getDao().getEntityManager();
                em.getTransaction().begin();
                em.persist(getAacc());
                em.getTransaction().commit();

                selectAllAaccs();
                setAacc(null);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade cadastrada!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Atividade não cadastrada!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void resultAacc() {
        selectAllAaccs();
    }

    public void selectAllAaccs() {
        resultSetAacc = new ArrayList<Aacc>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Aacc> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Aacc.class);
        setResultSetAacc(easyCriteria.getResultList());
    }

    public void excluirAacc() {
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Aacc> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Aacc.class);
        try {
            easyCriteria.andEquals("nome", getSelectedAacc().getNome());
            setAacc(easyCriteria.getResultList().get(0));
            em.getTransaction().begin();
            em.remove(getAacc());
            em.getTransaction().commit();
            selectAllAaccs();
            System.out.println("excluiu");
            
            setAacc(null);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Atividade excluída!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Atividade não excluída!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void atribuiPeriodo() {
        PeriodoManager pm = new PeriodoManager();
        getAacc().setIdPeriodo(pm.selecionaPeriodoList(getIdPeriodo()));
        System.out.println("ID PERIODO: " + getAacc().getIdPeriodo().getIdPeriodo());
    }

    public void atribuiClassificacao() {
        ClassificacaoAaccManager ca = new ClassificacaoAaccManager();
        getAacc().setIdClassificacao(ca.selecionaClassificacaoList(getIdClassificacao()));
    }

    public void atribuiDocumento() {
        CategoriaDocumentoManager cd = new CategoriaDocumentoManager();
        getAacc().setIdDocumento(cd.selecionaDocumentoList(getIdDocumento()));
    }

    public void dataHoraAtual() {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String s = f.format(new Date());
        getAacc().setDataInicio(new Date());
    }

    public List<Aacc> atividadPorUsuario() {
        listPorUsuario = new ArrayList<Aacc>();
        for (int i = 0; i < getResultSetAacc().size(); i++) {
            if (getResultSetAacc().get(i).getIdPeriodo().getIdPeriodo().equals(LoginManager.logado.getIdPeriodo().getIdPeriodo())) {
                getListPorUsuario().add(getResultSetAacc().get(i));
            }
        }
        return listPorUsuario;
    }

    public List<Aacc> filtroAaccPorPeriodo() {
        setaPeriodoAtividade();
        listPorPeriodo = new ArrayList<Aacc>();
        for (int i = 0; i < getResultSetAacc().size(); i++) {
            if (getResultSetAacc().get(i).getIdPeriodo().equals(p)) {
                getListPorPeriodo().add(getResultSetAacc().get(i));
            }
        }
        anexoImg = "C:\\Program Files\\Apache Software Foundation\\Apache Tomcat 7.0.34\\bin\\atividades\\15\\XfQMErGNfT.jpg";
        System.out.println("passou");
        System.out.println("PERIODO: " + getIdPeriodo());
        return listPorPeriodo;
    }

    public Aacc retornaAaccSelecionada(int idAacc) {
        Aacc atv = new Aacc();
        for (int i = 0; i < getResultSetAacc().size(); i++) {
            if (getResultSetAacc().get(i).getIdAACC().equals(idAacc)) {
                atv = getResultSetAacc().get(i);
            }
        }
        return atv;
    }

    public void setaPeriodoAtividade() {
        selectAllAaccs();
        System.out.println("PERIODO: " + getIdPeriodo());
        PeriodoManager pm = new PeriodoManager();
        p = pm.selecionaPeriodoList(getIdPeriodo());
    }

    /**
     * @return the aacc
     */
    public Aacc getAacc() {
        return aacc;
    }

    /**
     * @param aacc the aacc to set
     */
    public void setAacc(Aacc aacc) {
        this.aacc = aacc;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the periodo
     */
    public int getIdPeriodo() {
        return idPeriodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setIdPeriodo(int idPeriodo) {
        this.idPeriodo = idPeriodo;
    }

    /**
     * @return the classificacao
     */
    public int getIdClassificacao() {
        return idClassificacao;
    }

    /**
     * @param classificacao the classificacao to set
     */
    public void setIdClassificacao(int idClassificacao) {
        this.idClassificacao = idClassificacao;
    }

    /**
     * @return the documento
     */
    public int getIdDocumento() {
        return idDocumento;
    }

    /**
     * @param documento the documento to set
     */
    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    /**
     * @return the listAacc
     */
    public List<Aacc> getResultSetAacc() {
        return resultSetAacc;
    }

    /**
     * @param listAacc the listAacc to set
     */
    public void setResultSetAacc(List<Aacc> resultSetAacc) {
        this.resultSetAacc = resultSetAacc;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the filtroAacc
     */
    public List<Aacc> getFiltroAacc() {
        return filtroAacc;
    }

    /**
     * @param filtroAacc the filtroAacc to set
     */
    public void setFiltroAacc(List<Aacc> filtroAacc) {
        this.filtroAacc = filtroAacc;
    }

    /**
     * @return the selectedAacc
     */
    public Aacc getSelectedAacc() {
        return selectedAacc;
    }

    /**
     * @param selectedAacc the selectedAacc to set
     */
    public void setSelectedAacc(Aacc selectedAacc) {
        this.selectedAacc = selectedAacc;
    }

    /**
     * @return the listPorPeriodo
     */
    public List<Aacc> getListPorUsuario() {
        return listPorUsuario;
    }

    /**
     * @param listPorPeriodo the listPorPeriodo to set
     */
    public void setListPorUsuario(List<Aacc> listPorUsuario) {
        this.listPorUsuario = listPorUsuario;
    }

    /**
     * @return the listPorPeriodo
     */
    public List<Aacc> getListPorPeriodo() {
        return listPorPeriodo;
    }

    /**
     * @param listPorPeriodo the listPorPeriodo to set
     */
    public void setListPorPeriodo(List<Aacc> listPorPeriodo) {
        this.listPorPeriodo = listPorPeriodo;
    }

    /**
     * @return the p
     */
    public Periodo getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Periodo p) {
        this.p = p;
    }

    public String getAnexoImg() {
        return anexoImg;
    }

    public void setAnexoImg(String anexoImg) {
        this.anexoImg = anexoImg;
    }
    
}
