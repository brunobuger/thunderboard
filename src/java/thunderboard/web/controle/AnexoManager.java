/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Anexo;
  
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import thunderboard.modelo.entidades.Aacc;
import thunderboard.modelo.entidades.Evento;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "Upload")
@ViewScoped
public class AnexoManager implements Serializable {

    private Anexo anexo = new Anexo();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private javax.servlet.http.Part imagem;
    private UploadedFile fileup;
    private static int idUsuario;
    private static int idAacc;

    //private LoginManager userManager;

    public AnexoManager() {
        
    }
    
    public void enviarAtividade(FileUploadEvent event) {
        EntityManager em = getDao().getEntityManager();
        idAacc = (Integer) event.getComponent().getAttributes().get("aacc");
        idUsuario =(Integer) event.getComponent().getAttributes().get("usuario");
        
        if(idAacc > 0 && idUsuario > 0)
        {
        EasyCriteria<Evento> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Evento.class);
        easyCriteria.andEquals("idAACC", idAacc);
        easyCriteria.andEquals("idUsuario", idUsuario);
        try {
            Evento evento = easyCriteria.getResultList().get(0);
            fileup = event.getFile();
            InputStream in;
            try {
                in = new BufferedInputStream(fileup.getInputstream());
                File pastaAlvo = new File("atividades/"+idUsuario);
                if (!pastaAlvo.exists()) {
                    pastaAlvo.mkdir();
                }
                File file = new File(pastaAlvo.getPath() + "/" + getRandomName(10) + fileup.getFileName().substring(fileup.getFileName().indexOf(".")));
                FileOutputStream fout = new FileOutputStream(file);
                System.out.println(file.getAbsoluteFile());
                while (in.available() != 0) {
                    fout.write(in.read());
                }

                fout.close();
                em.getTransaction().begin();
                Anexo anexo = new Anexo();
                anexo.setIdEvento(evento);
                anexo.setImagem(file.getAbsolutePath());
                em.persist(anexo);
                em.getTransaction().commit();
            } catch (IOException ex) {
                Logger.getLogger(AnexoManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            FacesMessage msg = new FacesMessage("Sucesso", event.getFile().getFileName() + " salvo com sucesso.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Não existem eventos criados");
        }
        }

    }

    
    /**
     * @return the anexo
     */
    public Anexo getAnexo() {
        return anexo;
    }

    /**
     * @param anexo the anexo to set
     */
    public void setAnexo(Anexo anexo) {
        this.anexo = anexo;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the imagem
     */
    public javax.servlet.http.Part getImagem() {
        return imagem;
    }

    /**
     * @param imagem the imagem to set
     */
    public void setImagem(javax.servlet.http.Part imagem) {
        this.imagem = imagem;
    }

    /**
     * @return the file
     */
    public UploadedFile getFile() {
        return fileup;
    }

    /**
     * @param file the file to set
     */
    public void setFile(UploadedFile file) {
        this.fileup = file;
    }
    
    public String getRandomName(int len) {
        char[] chart = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        char[] senha = new char[len];

        int chartLenght = chart.length;
        Random rdm = new Random();

        for (int x = 0; x < len; x++) {
            senha[x] = chart[rdm.nextInt(chartLenght)];
        }

        return new String(senha);
    }
    
}
