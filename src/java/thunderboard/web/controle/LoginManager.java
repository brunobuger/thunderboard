/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Usuario;

/**
 *
 * @author Buger
 */
@ManagedBean(name = "Acesso")
@SessionScoped
public class LoginManager implements Serializable{
    private Usuario usuario = new Usuario();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    static Usuario logado;
    
    public LoginManager(){
        
    }
    
    public boolean doAutentica()
    {
        EntityManager em = dao.getEntityManager();
        try {
            em.getTransaction().begin();
            EasyCriteria<Usuario> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Usuario.class);
            try {
                easyCriteria.andEquals("email", usuario.getEmail());
                easyCriteria.andEquals("senha", usuario.getSenha());
                usuario = easyCriteria.getResultList().get(0);
            } catch(IndexOutOfBoundsException e)
            {
                usuario = new Usuario();
                System.out.println("Senha invalida.");
            }            
        } catch (Exception ex) {
            displayErro();
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            em.close();
        }
        
        if (usuario.getStatus()) {
            displaySucesso();
        } else {
            displayErro();
        }
        
        return usuario.getStatus();
    }
    
    public void displayErro() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR ,"Erro", "Senha incorreta!");  
        FacesContext.getCurrentInstance().addMessage(null, msg);        
    }
    
    public void displaySucesso() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()+"/thunder/index.xhtml");
            logado = usuario;
        } catch (IOException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getMenu()
    {
        String tipoMenu;
        if (usuario.getNivel() == 2) {
            tipoMenu = "menuDocente";
        } else {
            tipoMenu = "menuAluno";
        }
        return tipoMenu;
    }
    
    public void doLogout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/index.xhtml");
            logado = null;
        } catch (IOException ex) {
            System.out.println("Falha redirecionamento logoff de sessão.");
        }
    }
    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
}