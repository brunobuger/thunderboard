/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Evento;
import thunderboard.modelo.entidades.Periodo;
import thunderboard.modelo.entidades.Usuario;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "Aluno")
@SessionScoped
public class UsuarioManager implements Serializable {

    private Usuario usuario = new Usuario();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private Usuario selectedUsuario;
    private Usuario usuarioCad;
    private int idPeriodoCad;
    private int idPeriodoExc;
    private String nomeCad;
    private int idUsuarioExc;
    private String email;
    private String senhaPadrao;
    private String senha;
    private String confSenha;
    private List<Usuario> resultSetUsuario;
    private List<Usuario> filterUsuario;
    private Periodo p;
    private EmailManager emailManager = new EmailManager();

    public UsuarioManager() {
        setSenhaPadrao("thunderboard");
        selectAllUsuarios();
        setUsuario(LoginManager.logado);
    }

    public void cadastrarUsuario() {
        usuarioCad = new Usuario();
        getUsuarioCad().setNivel(Short.parseShort("" + 1));
        getUsuarioCad().setStatusToken(true);
        getUsuarioCad().setStatus(true);
        atribuiPeriodo();
        getUsuarioCad().setNome(getNomeCad());
        getUsuarioCad().setEmail(getEmail());
        getUsuarioCad().setSenha(getSenhaPadrao());
        
        try {
            if (getUsuario() != null) {
                EntityManager em = getDao().getEntityManager();
                em.getTransaction().begin();
                em.persist(getUsuarioCad());
                em.getTransaction().commit();
                System.out.println("cadastrou!!");
                
                getEmailManager().enviandoEmails(getUsuarioCad());
                selectAllUsuarios();
                
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Aluno cadastrado!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                
                limparCampos();
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Aluno não cadastrado!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void alterarUsuario() {
        try {

            if (getSenha().equals(getConfSenha())) {
                getUsuario().setSenha(getSenha());
            } else {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "As senhas estão diferentes!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }

            EntityManager em = getDao().getEntityManager();
            Usuario user = em.find(Usuario.class, LoginManager.logado.getIdUsuario());
            em.getTransaction().begin();
            user.setNome(getUsuario().getNome());
            user.setEmail(getUsuario().getEmail());
            user.setSenha(getUsuario().getSenha());
            em.getTransaction().commit();

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Dados atualizados com êxito!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            limparCampos();
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Erro ao alterar dados!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void excluirUsuario() {
        setUsuario(selecionaUsuarioList(getIdUsuarioExc()));
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Usuario> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Usuario.class);
        try {
            easyCriteria.andEquals("nome", getUsuario().getNome());
            setUsuario(easyCriteria.getResultList().get(0));
            em.getTransaction().begin();
            em.remove(getUsuario());
            em.getTransaction().commit();
            
            selectAllUsuarios();
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso", "Aluno excluído com êxito!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            limparCampos();
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Erro ao excluir aluno!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void atribuiPeriodo() {
        PeriodoManager pm = new PeriodoManager();
        getUsuarioCad().setIdPeriodo(pm.selecionaPeriodoList(getIdPeriodoCad()));
    }

    public void selectAllUsuarios() {
        resultSetUsuario = new ArrayList<Usuario>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Usuario> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Usuario.class);
        setResultSetUsuario(easyCriteria.getResultList());
    }

    public List<Usuario> filtrarUsuarios() {
        setaPeriodo();
        filterUsuario = new ArrayList<Usuario>();
        for (int i = 0; i < getResultSetUsuario().size(); i++) {
            if (getResultSetUsuario().get(i).getIdPeriodo().equals(p)) {
                getFilterUsuario().add(getResultSetUsuario().get(i));
            }
        }
        System.out.println("passou");
        System.out.println("PERIODO: " + getIdPeriodoExc());
        return filterUsuario;
    }

    public void setaPeriodo() {
        selectAllUsuarios();
        System.out.println("PERIODO: " + getIdPeriodoExc());
        PeriodoManager pm = new PeriodoManager();
        p = pm.selecionaPeriodoList(getIdPeriodoExc());
    }
    
    public Usuario selecionaUsuarioList(int idUsuario){
        Usuario u = new Usuario();
        for (int i = 0; i < getResultSetUsuario().size(); i++) {
            if (getResultSetUsuario().get(i).getIdUsuario().equals(idUsuario)) {
                u = getResultSetUsuario().get(i);
            }
        }
        return u;
    }
    
    public Usuario verificaEmail(String email){
        Usuario u = new Usuario();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<Usuario> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Usuario.class);
        try{
            easyCriteria.andEquals("email", email);
            u = easyCriteria.getResultList().get(0);
        }catch(Exception e){
            System.out.println("ERRO: " + e);
        }
        return u;
    }
    
    public void limparCampos(){
        setSenha("");
        setConfSenha("");
        setEmail("");
        setNomeCad("");
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the selectedUsuario
     */
    public Usuario getSelectedUsuario() {
        return selectedUsuario;
    }

    /**
     * @param selectedUsuario the selectedUsuario to set
     */
    public void setSelectedUsuario(Usuario selectedUsuario) {
        this.selectedUsuario = selectedUsuario;
    }

    /**
     * @return the resultUsuario
     */
    public List<Usuario> getResultSetUsuario() {
        return resultSetUsuario;
    }

    /**
     * @param resultUsuario the resultUsuario to set
     */
    public void setResultSetUsuario(List<Usuario> resultSetUsuario) {
        this.resultSetUsuario = resultSetUsuario;
    }

    /**
     * @return the filterUsuario
     */
    public List<Usuario> getFilterUsuario() {
        return filterUsuario;
    }

    /**
     * @param filterUsuario the filterUsuario to set
     */
    public void setFilterUsuario(List<Usuario> filterUsuario) {
        this.filterUsuario = filterUsuario;
    }

    /**
     * @return the periodoCad
     */
    public int getIdPeriodoCad() {
        return idPeriodoCad;
    }

    /**
     * @param periodoCad the periodoCad to set
     */
    public void setIdPeriodoCad(int idPeriodoCad) {
        this.idPeriodoCad = idPeriodoCad;
    }

    /**
     * @return the periodoExc
     */
    public int getIdPeriodoExc() {
        return idPeriodoExc;
    }

    /**
     * @param periodoExc the periodoExc to set
     */
    public void setIdPeriodoExc(int idPeriodoExc) {
        this.idPeriodoExc = idPeriodoExc;
    }

    /**
     * @return the p
     */
    public Periodo getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Periodo p) {
        this.p = p;
    }

    /**
     * @return the nome
     */
    public String getNomeCad() {
        return nomeCad;
    }

    /**
     * @param nome the nome to set
     */
    public void setNomeCad(String nomeCad) {
        this.nomeCad = nomeCad;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the confSenha
     */
    public String getConfSenha() {
        return confSenha;
    }

    /**
     * @param confSenha the confSenha to set
     */
    public void setConfSenha(String confSenha) {
        this.confSenha = confSenha;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senhaPadrao
     */
    public String getSenhaPadrao() {
        return senhaPadrao;
    }

    /**
     * @param senhaPadrao the senhaPadrao to set
     */
    public void setSenhaPadrao(String senhaPadrao) {
        this.senhaPadrao = senhaPadrao;
    }

    /**
     * @return the nomeExc
     */
    public int getIdUsuarioExc() {
        return idUsuarioExc;
    }

    /**
     * @param nomeExc the nomeExc to set
     */
    public void setIdUsuarioExc(int idUsuarioExc) {
        this.idUsuarioExc = idUsuarioExc;
    }

    /**
     * @return the usuarioCad
     */
    public Usuario getUsuarioCad() {
        return usuarioCad;
    }

    /**
     * @param usuarioCad the usuarioCad to set
     */
    public void setUsuarioCad(Usuario usuarioCad) {
        this.usuarioCad = usuarioCad;
    }

    /**
     * @return the emailManager
     */
    public EmailManager getEmailManager() {
        return emailManager;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }
}
