/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

/**
 *
 * @author vitor
 */
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import thunderboard.modelo.entidades.Usuario;

public class EmailManager {
    boolean estaEnviando = false;

    public void enviandoEmails(Usuario user) {

            String mensagem = "";
            mensagem += "" + user.getNome() + "\n\n";
            mensagem += "Dados para acesso:\n";
            mensagem += "E-mail: " + user.getEmail() + "\n";
            mensagem += "Senha: " + user.getSenha() + "\n";

            String mailSMTPServer = "smtp.gmail.com";
            String mailSMTPServerPort = "465";
            String de = "thunderboard.alert@gmail.com";
            String para1 = user.getEmail();

            Properties propriedades = new Properties();

            propriedades.put("mail.transport.protocol", "smtp");
            propriedades.put("mail.smtp.starttl.enable", "true");
            propriedades.put("mail.smtp.host", mailSMTPServer);
            propriedades.put("mail.smtp.auth", "true");
            propriedades.put("mail.smtp.user", de);
            propriedades.put("mail.debug", "true");
            propriedades.put("mail.smtp.port", mailSMTPServerPort);
            propriedades.put("mail.smtp.socketFactory.port", mailSMTPServer);
            propriedades.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            propriedades.put("mail.smtp.socketFactory.fallback", "false");

            Authenticator autenticador = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("thunderboard.alert@gmail.com", "openthunder");
                }
            };

            Session session;
            session = Session.getDefaultInstance(propriedades, autenticador);
            session.setDebug(true);

            Message msg = new MimeMessage(session);

            try {
                //Setando o destinatário  
                InternetAddress[] enderecos = {new InternetAddress(para1)};
                msg.setRecipients(Message.RecipientType.TO, enderecos);
                //Setando a origem do email  
                msg.setFrom(new InternetAddress(de));
                //Setando o assunto  
                msg.setSubject("Dados para acesso ThunderBoard");
                //Setando o conteúdo/corpo do email  
                msg.setContent(mensagem, "text/plain");

            } catch (Exception e) {
                System.out.println(">> Erro: Completar Mensagem");
                e.printStackTrace();
            }

            Transport tr;
            try {
                tr = session.getTransport("smtp"); //define smtp para transporte  
            /* 
                 *  1 - define o servidor smtp 
                 *  2 - seu nome de usuario do gmail 
                 *  3 - sua senha do gmail 
                 */
                tr.connect(mailSMTPServer, "thunderboard.alert@gmail.com", "openthunder");
                msg.saveChanges(); // don't forget this  
                //envio da mensagem  
                tr.sendMessage(msg, msg.getAllRecipients());
                tr.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block  
                System.out.println(">> Erro: Envio Mensagem");
                e.printStackTrace();
            }
    }
}
