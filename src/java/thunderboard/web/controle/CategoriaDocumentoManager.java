/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.CategoriaDocumento;

/**
 *
 * @author vitor
 */
@ManagedBean(name = "CategoriaDocumento")
@SessionScoped
public class CategoriaDocumentoManager implements Serializable {

    private CategoriaDocumento categoria = new CategoriaDocumento();
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private List<CategoriaDocumento> resultSetDocumento;
    private List<String> listDocumento;
    private String selecionado;

    public CategoriaDocumentoManager() {
        selectAllDocumentos();
    }
    
    public void selectAllDocumentos(){
        resultSetDocumento =  new ArrayList<CategoriaDocumento>();
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<CategoriaDocumento> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, CategoriaDocumento.class);
        setResultSetDocumento(easyCriteria.getResultList());
    }
    
    public List<String> autoCompletarPeriodo(String query){
        listDocumento = new ArrayList<String>();
        for (int i = 0; i < resultSetDocumento.size(); i++) {
            getListDocumento().add("" + resultSetDocumento.get(i).getTitulo());
        }
        return listDocumento;
    }

    public void cadastrarDocumento() {
        try {
            if (getCategoria() != null) {
                EntityManager em = getDao().getEntityManager();
                em.getTransaction();
                em.persist(getCategoria());
                em.getTransaction();
            }
        } catch (Exception e) {
        }
    }

    public void excluirDocumento(String nome) {
        EntityManager em = getDao().getEntityManager();
        EasyCriteria<CategoriaDocumento> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, CategoriaDocumento.class);
        try {
            easyCriteria.andEquals("nome", getCategoria().getTitulo());
            setCategoria(easyCriteria.getResultList().get(0));
            em.remove(getCategoria());
        } catch (Exception e) {
            setCategoria(new CategoriaDocumento());
            System.out.println("Atividade nao encontrada.");
        }
    }
    
    public CategoriaDocumento selecionaDocumentoList(int idDocumento){
        CategoriaDocumento d = new CategoriaDocumento();
        for (int i = 0; i < getResultSetDocumento().size(); i++) {
            if (getResultSetDocumento().get(i).getIdDocumento().equals(idDocumento)) {
                d =  getResultSetDocumento().get(i);
            }
        }
        return d;
    }

    /**
     * @return the categoria
     */
    public CategoriaDocumento getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(CategoriaDocumento categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the dao
     */
    public DAOBuger getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(DAOBuger dao) {
        this.dao = dao;
    }

    /**
     * @return the resultPeriodo
     */
    public List<CategoriaDocumento> getResultSetDocumento() {
        return resultSetDocumento;
    }

    /**
     * @param resultPeriodo the resultPeriodo to set
     */
    public void setResultSetDocumento(List<CategoriaDocumento> resultSetDocumento) {
        this.resultSetDocumento = resultSetDocumento;
    }

    /**
     * @return the listPeriodos
     */
    public List<String> getListDocumento() {
        return listDocumento;
    }

    /**
     * @param listPeriodos the listPeriodos to set
     */
    public void setListDocumento(List<String> listDocumento) {
        this.listDocumento = listDocumento;
    }

    /**
     * @return the selecionado
     */
    public String getSelecionado() {
        return selecionado;
    }

    /**
     * @param selecionado the selecionado to set
     */
    public void setSelecionado(String selecionado) {
        this.selecionado = selecionado;
    }
}
