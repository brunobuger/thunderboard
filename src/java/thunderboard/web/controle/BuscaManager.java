/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderboard.web.controle;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import thunderboard.modelo.DAO.DAOBuger;
import thunderboard.modelo.entidades.Aacc;

/**
 *
 * @author Buger
 */
@ManagedBean(name="Busca")
@RequestScoped
public class BuscaManager implements Serializable{

    /**
     * Creates a new instance of BuscaManager
     */
    private List<Aacc> atividades;
    private LoginManager userManager;
    private DAOBuger dao = new DAOBuger("ThunderBoardPU");
    private String campoBusca;
    
    public BuscaManager() {
        atividades = new ArrayList<Aacc>();
        userManager = (LoginManager) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }
    
    public void doBusca()
    {
        EntityManager em = dao.getEntityManager();
        em.getTransaction().begin();
        EasyCriteria<Aacc> easyCriteria = EasyCriteriaFactory.createQueryCriteria(em, Aacc.class);
        easyCriteria.andEquals("idUsuario", userManager.getUsuario().getIdUsuario());
        easyCriteria.andStringLike("nome", "%"+getCampoBusca()+"%");
        atividades = easyCriteria.getResultList();
        em.close();
    }

    /**
     * @return the campoBusca
     */
    public String getCampoBusca() {
        return campoBusca;
    }

    /**
     * @param campoBusca the campoBusca to set
     */
    public void setCampoBusca(String campoBusca) {
        this.campoBusca = campoBusca;
    }

    /**
     * @return the atividades
     */
    public List<Aacc> getAtividades() {
        return atividades;
    }

    /**
     * @param atividades the atividades to set
     */
    public void setAtividades(List<Aacc> atividades) {
        this.atividades = atividades;
    }
    
}
